These binaries are used by training modules 16 and 17.
Students download them to a local ~/DevOps/wegood directory using their Gitlab URLs,
and then install them in their local Maven repository, using these commands:

```
mvn install:install-file -Dfile=~/DevOps/wegood/wegood.stub-1.0.jar \
-DgroupId=com.cliffberg.devopsforagilecoaches -DartifactId=wegood.stub -Dversion=1.0 \
-Dpackaging=jar -DgeneratePom=true

mvn install:install-file -Dfile=~/DevOps/wegood/wegood.cdk-1.0.jar \
-DgroupId=com.cliffberg.devopsforagilecoaches -DartifactId=wegood.cdk -Dversion=1.0 \
-Dpackaging=jar -DgeneratePom=true
```
